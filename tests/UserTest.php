<?php

namespace App\Tests;

use App\Entity\User;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;

class UserTest extends ApiTestCase
{
    use RefreshDatabaseTrait;

    public function testCreateUser()
    {
        static::createClient()->request('POST', '/api/users', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => [
                'password' => 'password',
                'firstname' => 'john',
                'lastname' => 'Mckenzy',
                'login' => 'john@Mckenzy.com',
            ]
        ]);
        $this->assertResponseStatusCodeSame(201);
    }

}